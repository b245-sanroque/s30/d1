db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },
  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },
  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },
  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);


// [SECTION] MongoDB Aggregation
/*
	- to generate and perform operations to create filtered results that helps us analyze the data

	- using aggregate method:
		- the "$match" is used to pass the documents that meet the specified condition/s to the next stage or aggregation process

		SYNTAX:
			{ $match: {field: value} }
*/

*ok*
db.fruits.aggregate([ {$match : {onSale: true} } ]); // returns fruits onSale: apple, banana, kiwi

	/*
		- the "$group" is used to group the elements together and firld-value the data from the grouped element

		SYNTAX:
			{ $group: {_id: "fieldSetGroup"} }
	*/

*ok*
db.fruits.aggregate([ 
	{$match : {onSale: true} },
	{$group : {_id: "$supplier_id"} }  //grouped into two: 1.0 and 2.0
]); 

*ok*
db.fruits.aggregate([ 
{$match: {onSale: true} },
{$group: {	_id: "$supplier_id", 
			totalFruits: {$sum: "$stock"}} } // displays two grouped field sets with total stocks per group
]); 

*ok*
db.fruits.aggregate([ 
{$match: {	onSale: true, 
			name: "Apple"} },
{$group: {	_id: "$supplier_id", 
			totalFruits: {$sum: "$stock"} } } // returns stock value of Apple onSale
]);

		// Mini Activity
			// First get all the fruits that are color Yellow then grouped them into their respective supplier and their available stocks.

			db.fruits.aggregate([ 
				{$match: {color: "Yellow"} },
				{$group: {	_id: "$supplier_id", 
							totalFruits: {$sum: "$stock"}} }
			]);

	// Field Projection with Aggregation
	/*
		- $project can be used when aggregating data to include/exclude from the returned result

		SYNTAX:
			{project: {field: 1/0}}
	*/

*ok*
db.fruits.aggregate([ 
	{$match: 	{ color: "Yellow"} },
	{$group: 	{ 	_id: "$supplier_id", 
					totalFruits: {$sum: "$stock"} } },
	{$project: 	{_id: 1} } // if 1: only projects "_id" field, 
						  // if 0: excludes "_id" and projects sum to fruits instead
]);


	// Sorting aggregated results
	/*
		- $sort can be used to change the order of the aggregated result
		- the value in sort"
		  1 	- displays lowest to highest
		 -1		- highest to lowest
	*/

*ok*
db.fruits.aggregate([ 
	{$match : 	{onSale: true} },
	{$group : 	{	_id: "$supplier_id",
					totalFruits: {$sum: "$stock"} } },
	{$sort : 	{totalFruits: 1} }
]);


	// aggregating the result based on an array fields
	/*
		the $unwind deconstruct an array field from a collection/ field with an array value to output a result
	*/

*ok*
db.fruits.aggregate([
	{$unwind: "$origin"},
	{$group: {	_id: "$origin", 
				fruits: {$sum: 1} // displays no. of fruit per country
				// fruits: {$sum: "$stock"} //sum of total stock per country
		}
	}
]);


// [SECTION] - other aggregate stages 

	// $count all yellow fruits
*ok*
	db.fruits.aggregate([
		{$match: {color: "Yellow"} },
		{$count: "Yellow Fruits"}  //creates field set, displays all yellow fruits
	]);


	// $avg gets the average value of the stocks
*ok*
	db.fruits.aggregate([
		{$match: {color: "Yellow"} },
		{$group: {	_id: "$color", 
					avgYellow: {$avg: "$stock"} } } //creates field set avgYellow, returns avg stock on  yellow colored fruits
		]);	


	// $min and $max
*ok*
		db.fruits.aggregate([
			{$match: {color: "Yellow"} },
			{$group: {	_id: "$color", 
						lowestStock: {$min: "$stock"} } }// returns value of lowest stock yellow fruit
		]);	

*ok*
		db.fruits.aggregate([
			{$match: {color: "Yellow"} },
			{$group: {	_id: "$color", 
						highestStock: {$max: "$stock"} } }// returns value of highest stock yellow fruit
		]);